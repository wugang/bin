#!/usr/bin/env bash
#
# Description: Start day script to update packages, check weather, and receive a fortune
# Dependencies: fortune-mod, curl, someday figlet and jq
# Resources:
#

Weather() {
	  curl wttr.in/Nijmegen?0
}

Moon() {
	  curl -s wttr.in/Moon |head -23 | tail -60
}

Fortune() {
    fortune -s
}

#Begin_Tail() {
#    echo
#	  Weather
#	  echo
#	  Moon
#	  echo
#	  echo "Daily Fortune"
#	  echo
#	  Fortune
#	  echo
#}

#Execute_Tail() {
#    termite -e Begin_Tail
#}

Begin_Head() {
    #	figlet -c Morning #Chinese not working in figlet
	  echo
	  echo "早上好, Alec"
	  echo
	  echo "Synchronizing Packages"
	  trizen -Syu --noconfirm
	  echo
    Weather
    echo
    Moon
    echo
#    echo "Daily Fortune"
    echo
    Fortune | cowsay -f tux
    echo
}

Begin_Head
