#!/usr/bin/env bash

help() { echo "HELP"; exit 1; }
[ $# -eq 0 -o $# -gt 256 ] && help
for opt in $@; do case "${opt:0:2}" in --*) help;; esac; done

while getopts c:vh OPT; do case "${OPT}" in
                               c) opt="${OPTARG}";;
                               h) help; exit 1;;
                           esac; done

[ -n $opt ] && echo "${0//*\//}: got opt=\"${opt}\""
exit 0
