#!/usr/bin/env bash
#
# Description: change monitor configuration to only laptop screen then reset background
# Dependencies: xrandr pywal wpgtk
# Resources:

xrandr --output VIRTUAL1 --off --output eDP1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP1 --off --output DP2-1 --off --output DP2-2 --off --output DP2-3 --off --output HDMI2 --off --output HDMI1 --off --output DP2 --off

wpg -s $(wpg -c)
