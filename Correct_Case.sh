#!/usr/bin/perl
#
# Description: Format to proper case and replace spaces with _
# Resources:
# https://www.linuxquestions.org/questions/programming-9/shell-script-for-converting-filenames-to-proper-case-436406/

foreach (@ARGV) {
  $was = $_;
  $_ = join ' ', map {ucfirst(lc($_))} split /\s+/, $_;
  rename($was,$_) or warn "Couldn't rename $was: $!";
}
