#!/usr/bin/env bash
#
# Description: Clean caches, logs, & trash, removes unused packages, orphaned package files, refreshes pacman keyring, cleans firefox and chromium profiles, report storage usage
# Dependencies: profile-cleaner paccache
# Resources:
# https://averagelinuxuser.com/clean-arch-linux/
# https://bbs.archlinux.org/viewtopic.php?id=33336
# https://github.com/graysky2/profile-cleaner

Clean() {
    echo
    echo "正在刷新..."
    echo
    echo "Cleaning uninstalled packages from pacman cache"
    sudo pacman -Sc
    echo
    echo "Cleaning orphaned librares not required"
    sudo pacman -Rns $(pacman -Qtdq)
    echo
    echo "Pruning cache to 1 version and removing uninstalled packages"
    sudo paccache -rk 1 && paccache -ruk 0
    echo
    echo "Cleaning local cache"
    rm -rf ~/.cache/*
    echo
    echo "Cleaning trash"
    rm -rf ~/.local/share/Trash/files/*
    echo
    #https://bbs.archlinux.org/viewtopic.php?id=138722
    echo "Cleaning log files"
    sudo rm -rf /var/log/journal/*
    echo
    #https://bbs.archlinux.org/viewtopic.php?id=195139
    echo "Updating pacman keyring"
    sudo pacman-key --populate archlinux && sudo pacman-key --refresh-keys
    echo
    echo "Cleaning chromium configuration"
    profile-cleaner c
    echo
    #https://askubuntu.com/questions/1038150/why-does-chromium-take-up-1-gb-in-my-config-and-can-i-reduce-that-size
    echo "Cleaning chromium file system bloat"
    sudo rm -rf ~/.config/chromium/Default/File System/
    echo "Cleaning firefox configuration"
    profile-cleaner f
    echo "Cleaning thunderbird configuration"
    profile-cleaner t
    echo
}

Clean
