#!/usr/bin/env bash
#
# Description: Save selection to screenshot using UTC ISO-8601 Y-m-dZT
# Dependencies: slop, imagemagick
# Resources:
# https://en.wikipedia.org/wiki/ISO_8601
# https://github.com/naelstrof/slop

utctime="$(date -u +"%Y-%m-%dT%TZ")"

read -r G < <(slop -f "%g")
import -window root -crop $G ~/tmp/"${utctime}".png

