#!/usr/bin/env bash
#
# Description: Blur and lock the screen
# Dependencies: imagemagick and i3lock
# Resources:
# https://www.reddit.com/r/unixporn/comments/821avm/2bwm_locked/ 
# https://github.com/gawlk/bin 

tmp="/tmp/screen.png"
tmp2="/tmp/lock.png"
tmp3="/tmp/bg.png"
lock="${HOME}/.bin/lock.png"
#colr="${HOME}/.colors"

letters=6
fontHeight=7
fontWidth=5
padding=64

height=$(( $fontHeight + $padding ))
width=$(( ( ( $fontWidth * $letters ) - 1 ) + $padding )) 

#wht=`cat $colr | grep "wht" | awk '{ print $3 }'`
#blk=`cat $colr | grep "blk" | awk '{ print $3 }'`

# Take a screenshot
import -window root "$tmp"

# Blur it
convert "$tmp" -scale 10% -scale 1000% "$tmp"
convert "$tmp" -fill "$wht" -colorize 10% "$tmp"

# Change label's color
#convert "$lock" -fill $wht -opaque '#ffffff' "$tmp2"

# Create label's background
#convert -size ${width}x${height} xc:$blk "$tmp3"

# Merge
#convert "$tmp3" "$tmp2" -gravity center -composite "$tmp2"
#convert "$tmp" "$tmp2" -gravity center -composite "$tmp"

# Lock the screen
i3lock -e -u -i "$tmp"
