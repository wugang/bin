#!/usr/bin/env bash
#
# Description: swf to png to single pdf
# Dependencies: swftools imagemagick
# https://aur.archlinux.org/packages/swftools/
#
# Resources:
# https://unix.stackexchange.com/questions/17608/swf-to-pdf-conversion
# https://stackoverflow.com/questions/18800101/swfrender-swftools-batch-output-to-png#29464899

#US Letter Size page that is at least marginally readable (about 200 dpi)
for x in *.swf; do
    swfrender -X 1836 -Y 2376 ${x} -o ${x%.swf}.png
done

