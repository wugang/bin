#!/usr/bin/env bash
#
# Description: change monitor configuration to only docked monitor then reset background
# Dependencies: xrandr pywal wpgtk
# Resources:

xrandr --output VIRTUAL1 --off --output eDP1 --off --output DP1 --off --output DP2-1 --primary --mode 2560x1440 --pos 0x0 --rotate normal --output DP2-2 --off --output DP2-3 --off --output HDMI2 --off --output HDMI1 --off --output DP2 --off

wpg -s $(wpg -c)
