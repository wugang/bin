#!/usr/bin/env bash
#
# Description: installation script for all packages on new install
# Dependencies:
#
# Resources:
# https://stackoverflow.com/questions/5417979/batch-rename-sequential-files-by-padding-with-zeroes
# http://www.walkingrandomly.com/?p=2850

#package install and configuration

#download dotfiles from repo & stow; necessary to do before installing all of
#the software, otherwise there will be conflicts with existing files
git clone https://gitlab.com/wugang/dotfiles.git ~/Dotfiles && cd Dotfiles && stow *
&&
cd

#install trizen
git clone https://aur.archlinux.org/trizen.git
&&
cd trizen
&&
makepkg -si
&&
cd
&&

#update pacman & trizen
sudo pacman -Syyu &&
trizen -Syyu &&

#install packages sudo pacman -S acpi adobe-source-code-pro-fonts arandr ark
trizen -S -q aspell-en atool biber breeze breeze-gtk cantor calibre calligra-plan chromium compton cowsay curl dolphin dolphin-plugins darktable dia emacs exfat-utils exa filelight feh ffmpegthumbs fortune-mod ghostscript gksu gnupg gnucash graphviz gvfs gwenview htop i3 inkscape jq kalgebra kalzium kapptemplate kbackup kcalc kcolorchooser kcron kdenetwork-filesharing kdesdk-thumbnailers kdegraphics-thumbnailers keepassxc kig kimageformats kmines kmplot knights kompare konqueror kphotoalbum krdc krename krita kruler krusader ksystemlog kturtle kvantum-qt5 ibus-rime ibus-qt lame linux-headers maim marble mpd minuet mpv ncmpcpp ncdu neovim newsboat nnn noto-fonts-cjk pacman-contrib pandoc parley partitionmanager powertop python-pip pv qutebrowser r ranger raw-thumbnailer redshift rocs rofi rtorrent ruby samba sdcv step stow syncthing syncthing-gtk taglib texlive-core thunderbird tree ttf-dejavu umbrello virtualbox virtualbox-host-modules-arch virtualbox-guest-utils virtualbox-guest-iso wine wqy-bitmapfont wqy-microhei xcb-util-xrm xf86-input-synaptics xorg-xbacklight xorg-xset youtube-viewer zathura zathura-pdf-poppler zsh annie auctex create_ap displaylink emacs-ess gtop lynis mps-youtube neofetch nerd-fonts-complete otf-overpass paper-icon-theme-git pdftk pearl-rename peek plantuml profile-cleaner rambox-bin sxiv synology-drive taskjuggler tcl tk ttf-fira-code ttf-font-awesome-4 ttf-material-icons virtualbox-ext-oracle wpgtk-git xbanish xcape xcb-util-xrm xppaut --noconfirm

&&

#install spacemacs for emacs
git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d

&&

#install oh-my-zsh
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k

&&

#move data from drive back to device

&&

#tune power consumption
sudo powertop --auto-tune

&&

sudo powertop -c

&&
#configure systemd services
#disable lightdm login manager
#sudo systemctl disable lightdm.service
#&&
#sudo systemctl enable samba smb nmb
#&&
#sudo systemctl start samba smb nmb
&&
sudo systemctl enable syncthing@wugang.service
&&
sudo systemctl start syncthing@wugang.service
