#!/usr/bin/env bash
#
# Description: Remove spaces recursively from files and replace with _
# Dependencies: rename/prename
# Resources:
# https://stackoverflow.com/questions/2709458/how-to-replace-spaces-in-file-names-using-a-bash-script#2709619
# https://unix.stackexchange.com/questions/5412/lowercasing-all-directories-under-a-directory

#rename directories first
find -name "* *" -type d | perl-rename 's/ /_/g'
#next rename files
find -name "* *" -type f | perl-rename 's/ /_/g'
#lowercase directories
find -name "* *" -type d | perl-rename 's!/([^/]*/?)$!\L/$1!'
#lowercase files
find -name "* *" -type f | perl-rename 's!/([^/]*/?)$!\L/$1!'


