#!/usr/bin/env bash
#
# Description: Save screenshot using UTC ISO-8601 Y-m-dZT
# Dependencies: maim
# Resources:
# https://en.wikipedia.org/wiki/ISO_8601 

utctime="$(date -u +"%Y-%m-%dT%TZ")"

maim ~/tmp/"${utctime}".png
