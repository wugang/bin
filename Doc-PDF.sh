#!/bin/bash
#
# Description: Convert Doc or Docx to PDF
# Dependencies: libreoffice
# Resources:
# https://ask.libreoffice.org/en/question/6571/whats-the-best-way-to-batch-convert-odf-to-pdf-on-windows/
location=${PWD}

#/*.doc /**/*.doc /*.docx /**/*.docx
for f in *.doc *.docx
do
	echo "Processing $f file..."
	libreoffice --headless --convert-to pdf:writer_pdf_Export $f
done
