#!/usr/bin/env bash
#
# Description: pad numbers with leading zeros
# Dependencies:
#
# Resources:
# https://stackoverflow.com/questions/5417979/batch-rename-sequential-files-by-padding-with-zeroes
# http://www.walkingrandomly.com/?p=2850

num=`expr match "$1" '[^0-9]*\([0-9]\+\).*'`
paddednum=`printf "%03d" $num`
echo ${1/$num/$paddednum}
