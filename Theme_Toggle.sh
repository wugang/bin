#!/bin/sh
#
# Description:
# - A bash script that checks to see if theme needs set, then 
#   what time it is to set light or dark theme
# Dependencies:
# - wal, wpgtk
# Resources:

uptime='cat /proc/uptime'
ctime='date +%k%M'

if [ "$uptime"<"1000" ]
then
	wpg -m
elif [ "$ctime">"800" ] || [ "$ctime"<"1830" ]
then
	wpg -LA $(wpg -c) && wpg -s $(wpg -c)
elif [ "$ctime"<"800" ] || [ "$ctime">"1830" ]
then 
	wpg -A $(wpg -c) && wpg -s $(wpg -c)
fi
