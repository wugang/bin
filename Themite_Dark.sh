#!/usr/bin/env bash
#
# Description: switch terminal and firefox homepage themes

# define themite as python file
themite="python /home/wugang/Dotfiles/themite/.config/themite/themite.py"

# change termite theme
${themite} -ttheme spacemacsdark

# remove active css file
rm ~/Notes/Basics/browser/start.css

# replace css file with theme
cp ~/Notes/Basics/browser/Homepage_Theme_Dark.css ~/Notes/Basics/browser/start.css
