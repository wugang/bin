#!/usr/bin/env bash
#
# Description: Stallman Freedom Index
# Dependencies: Pacman
# Resources:
# https://www.reddit.com/r/archlinux/comments/7ke32m/get_your_freedom_index/
# https://github.com/vmavromatis/absolutely-proprietary

#only gpl
GPL() {
    echo -n "Your Stallman Freedom index is " && echo "scale=2;" $( pacman -Qi | grep -c -e 'GPL') \* 100 / $( pacman -Qi | grep -c 'Name' ) | bc -l
}

#foss licenses: gpl, bsd, mit
FOSS() {
	  echo -n "Your FOSS Freedom index is " && echo "scale=2;" $( pacman -Qi | grep -c -e 'GPL' -e 'MIT' -e 'BSD' ) \* 100 / $( pacman -Qi | grep -c 'Name' ) | bc -l
}

#all 'free' licenses in addition to above: custom:University of Illinois/NCSA Open Source License, MPL (Mozilla public license), Apache and variants, ISC, custom:none

ALL() {
	  echo -n "Your Free Freedom index is at least " && echo "scale=2;" \
	                                                         $(pacman -Qi | grep -e '^Licenses' | \
        	                                                       grep -cE 'GPL|BSD|MIT|APACHE|MPL|PUBLIC|NCSA|NONE|NoCopywrite|ISC|CDDL|SIL|OFL' ) \
	                                                         \* 100 / $(pacman -Qi | grep -c 'Name' ) | bc -l
}

if [ "$1" = "-g" ]; then
	GPL
elif [ "$2" = "-a" ]; then
	ALL
fi

