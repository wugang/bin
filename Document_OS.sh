#!/usr/bin/env bash
#
# Description: Document OS: package documentation, base processes documentation
# Dependencies: pacgraph pscircle pacman
# Resources:
# https://en.wikipedia.org/wiki/ISO_8601
# http://kmkeen.com/pacgraph/
# https://gitlab.com/mildlyparallel/pscircle

# TODO
# Add function to document most used commands
# https://old.reddit.com/r/linux/comments/8vzr3y/whats_the_most_frequent_terminal_command_you_use/
# Add function to document most used packages

# Current Time
utctime="$(date -u +"%Y-%m-%dT%TZ")"

# Documentation Directory
Doc_Dir=/home/wugang/Documents/Basics/OS_Documentation/
# Overview File
Overview=_Overview.org

# Font
font="Nimbus Sans"
font_size=20
#set -e

# Color Variables: Spacemacs Light
foreground="655370"
foreground_bold="655370"
background="fbf8ef"
cursor="#1951d"
cursor_foreground="b1951d"
# Black, Gray, Silver, White
color0="212026"
color8="655370"
color7="655370"
color15="f8f8f8"
# Red
color1="f2241f"
color9="e0211d"
# Green
color2="67b11d"
color10="29422d"
# Yellow
color3="b1951d"
color11="b1951d"
# Blue
color4="3a81c3"
color12="2d4252"
# Purple
color5="a31db1"
color13="5d4d7a"
# Teal
color6="2d9574"
color14="2d9574"
# Extra colors
color16="ffa500"
color17="b03060"
color18="282828"
color19="444155"
color20="b8b8b8"
color21="e8e8e8"

main() {
    echo "Graphing Packages"
    Package_Graph
    echo "Generating Package List"
    Package_List
    echo "Graphing Processes"
    Process_Graph
    echo "Analyzing Package Licensing"
    Licensing
    echo "Analyzing Command Use"
    Commands
    echo "Screening Desktop"
    FS_Origin
    echo "Grepping Filesystem Origin"
    Screenshot
    echo "Documentation Complete"
}

# Generate Package Graph
Package_Graph() {
    pacgraph \
        --background=#$background \
        --dep=#$color4 \
        --top=#$color1 \
        --link=#$color8 \
        --point=10 50 \
        --f ~/Dotfiles/Documentation/"${utctime}_Pacgraph"
    #remove generated .svg file
    rm -rf "${Doc_Dir}""${utctime}_Pacgraph".svg
}

# Store explicitly installed packages to text file && store list of all packages and their respective sizes
# Does this list aur packages?
# https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Listing_packages
Package_List() {
    pacman -Qe && > expac -s -H M "%-30n %m" | sort -hk 2 "${Doc_Dir}""${utctime}_Package_List".txt
}

# Generate Process Graph
Process_Graph() {
    pscircle \
	      --root-pid=0 \
	      --max-children=90 \
	      --tree-sector-angle=6.245 \
	      --tree-rotate=true \
	      --tree-rotation-angle=0.2 \
	      --tree-center=-750:-200 \
	      --cpulist-center=1000:200 \
	      --memlist-center=1000:400 \
	      --background-color=$background \
        --link-convexity=0.5 \
        --link-width=0.5 \
	      --link-color-min=$color8 \
	      --link-color-max=$color8 \
        --dot-radius=4 \
        --dot-border-width=1 \
	      --dot-color-min=$color2 \
	      --dot-color-max=$color2 \
        --tree-radius-increment=250 \
	      --tree-font-size=$font_size \
	      --tree-font-color=$foreground \
	      --tree-font-face="${font}" \
	      --toplists-font-size=$font_size \
	      --toplists-font-color=$foreground \
	      --toplists-pid-font-color=$foreground_bold \
	      --toplists-font-face="${font}" \
        --toplists-column-padding=30 \
	      --toplists-row-height=20 \
	      --toplists-bar-height=10 \
	      --toplists-bar-background=$color8 \
	      --toplists-bar-color=$color5 \
        --output-height=1800 \
        --output-width=3200 \
        --output="${Doc_Dir}""${utctime}_PScircle".png
    }

Screenshot() {
    maim "${Doc_Dir}""${utctime}".png
}

Licensing() {
    echo -n "* License Indexes" >> "${Doc_Dir}""${utctime}""${Overview}"
    printf "\n" "\n" >> "${Doc_Dir}""${utctime}"_Overview.org
    echo -n "*Stallman Freedom Index*" >> "${Doc_Dir}""${utctime}""${Overview}"
    printf "\n" >> "${Doc_Dir}""${utctime}""${Overview}"

    #gpl license
    echo -n "Your Stallman Freedom index is " >> "${Doc_Dir}""${utctime}""${Overview}" \
        && echo "scale=2;" \
                $( pacman -Qi | grep -c -e 'GPL') \* 100 / \
                $( pacman -Qi | grep -c 'Name' ) \
            | bc -l >> "${Doc_Dir}""${utctime}""${Overview}"

    printf "\n" >> "${Doc_Dir}""${utctime}""${Overview}"
    echo -n "Stallman Freedom Index is calculated by dividing the number of GPL packages installed versus the overall packages installed." >> "${Doc_Dir}""${utctime}""${Overview}"
    printf "\n" >> "${Doc_Dir}""${utctime}""${Overview}"
    echo -n "*FOSS Index*" >> "${Doc_Dir}""${utctime}""${Overview}"
    printf "\n" >> "${Doc_Dir}""${utctime}""${Overview}"

    #foss licenses: gpl, bsd, mit
	  echo -n "Your FOSS index is " >> "${Doc_Dir}""${utctime}""${Overview}" \
        && echo "scale=2;" \
                $( pacman -Qi | grep -c -e 'GPL' -e 'MIT' -e 'BSD' ) \* 100 / \
                $( pacman -Qi | grep -c 'Name' ) \
            | bc -l >> "${Doc_Dir}""${utctime}""${Overview}"

    printf "\n" >> "${Doc_Dir}""${utctime}""${Overview}"
    echo -n "FOSS Index is calculated by dividing the number of GPL, MIT, & BSD packages installed versus the overall packages installed." >> "${Doc_Dir}""${utctime}""${Overview}"
    printf "\n" >> "${Doc_Dir}""${utctime}""${Overview}"
    echo -n "*Free License Index*" >> "${Doc_Dir}""${utctime}""${Overview}"
    printf "\n" >> "${Doc_Dir}""${utctime}""${Overview}"

    #all 'free' licenses in addition to above: custom:University of Illinois/NCSA Open Source License, MPL (Mozilla public license), Apache and variants, ISC, custom:none
	  echo -n "Your Free License index is at least " >> "${Doc_Dir}""${utctime}""${Overview}" \
        && echo "scale=2;" \
	              $(pacman -Qi | grep -e '^Licenses' | \
        	            grep -cE 'GPL|BSD|MIT|APACHE|MPL|PUBLIC|NCSA|NONE|NoCopywrite|ISC|CDDL|SIL|OFL' ) \
	              \* 100 / $(pacman -Qi | grep -c 'Name' ) \
            | bc -l >> "${Doc_Dir}""${utctime}""${Overview}"

    printf "\n" >> "${Doc_Dir}""${utctime}""${Overview}"
    echo -n "Free License Index is calculated by dividing the number of GPL, BSD, MIT, APACHE, MPL, PUBLIC, NCSA, NONE, NoCopywrite, ISC, CDDL, SIL, & OFL packages installed versus the overall packages installed." >> "${Doc_Dir}""${utctime}""${Overview}"
}

Commands() {
    printf "\n" >> "${Doc_Dir}""${utctime}""${Overview}"

    echo -n "* Frequently Used Commands" >> "${Doc_Dir}""${utctime}""${Overview}"

    printf "\n" >> "${Doc_Dir}""${utctime}""${Overview}"

    history | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a; }' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl | head -n10 >> "${Doc_Dir}""${utctime}""${Overview}"

    printf "\n" "\n" >> "${Doc_Dir}""${utctime}""${Overview}"

    history | tr -s ' ' | cut -d ' ' -f3 | sort | uniq -c | sort -n | tail | perl -lane 'print $F[1], "\t", $F[0], " ", "▄" x ($F[0] / 12)' >> "${Doc_Dir}""${utctime}""${Overview}"

    printf "\n" "\n" >> "${Doc_Dir}""${utctime}""${Overview}"
}

FS_Origin() {
    echo -n "* Filesystem Origin" >> "${Doc_Dir}""${utctime}""${Overview}"

    printf "\n" "\n" >> "${Doc_Dir}""${utctime}""${Overview}"

    tune2fs -l /dev/sda1 | grep 'Filesystem created:' >> "${Doc_Dir}""${utctime}""${Overview}"
}

main
